import { reactive, computed } from 'vue'

export default function useAddress () {
  const addressState = reactive({
    addressNameToEdit: null,
    from: {
      title: 'From',
      road: '3 Villeneuve',
      zipCode: '44130',
      city: 'Notre dame des Landes',
      complement: 'ZAD',
      country: 'Loire Atlantique'
    },
    to: {
      title: 'To',
      road: 'rue riquet',
      zipCode: '31000',
      city: 'Toulouse',
      state: 'France'
    },
    fields: [{
      name: 'road',
      label: 'Road',
      title: 'Road',
      type: 'text',
      style: {
        color: 'red'
      }
    }, {
      name: 'zipCode',
      label: 'Zip Code',
      title: 'Zip Code',
      type: 'number',
      style: {
        backgroundColor: 'green'
      }
    }, {
      name: 'city',
      label: 'City',
      title: 'City',
      type: 'text'
    }, {
      name: 'complement',
      label: 'Complement',
      title: 'Complement',
      type: 'text'
    }, {
      name: 'country',
      label: 'Country',
      title: 'Country',
      type: 'text'
    }, {
      name: 'state',
      label: 'State',
      title: 'State',
      type: 'text'
    }],
    coordinates: null,
    loading: false,
    currentCoordinate: null
  })

  const addressToEdit = computed({
    get () {
      if (!addressState.addressNameToEdit) return null
      return addressState[addressState.addressNameToEdit]
    },
    set (v) {
      addressState[addressState.addressNameToEdit] = v
    }
  })

  async function fetchCoordinates (address) {
    addressState.loading = true
    try {
      const response = await fetch(
        'https://api-adresse.data.gouv.fr/search/?q=' +
        address.road + ' ' + address.zipCode + ' ' + address.city + ' '
      )
      const json = await response.json()
      json.features.forEach(currentFeature => {
        currentFeature.geometry.coordinates.reverse()
      })
      addressState.coordinates = json.features
    } catch {
      // do nothing
    }
    addressState.loading = false
  }

  return {
    addressState,
    addressToEdit,
    fetchCoordinates
  }
}
