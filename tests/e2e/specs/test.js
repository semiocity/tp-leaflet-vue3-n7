// https://docs.cypress.io/api/introduction/api.html

describe('My First Test', () => {
  it('Visits the app root url', () => {
    cy.visit('/')
    cy.contains('h1', 'Welcome to Your Vue.js App')
  })
  it('Visits the about page url', () => {
    cy.visit('/about')
    cy.contains('h1', 'This is an about page')
  })
  it('Visits the search page url', () => {
    cy.visit('/search')
    cy.get('button').contains('Adresse de destination').click()

    // modifier ville code postal
    cy.get('input:nth-of-type(2)')
      .clear()
      .type('31870')
    cy.get('input:nth-of-type(3)')
      .clear()
      .type('Lagardelle')

    // fermer la recherche
    cy.get('button').contains('Close').click()

    // lancer la recherche
    cy.get('button').contains('Chercher Adresse de destination').click()
    cy.wait(500)

    // vérifier pop up
    cy.get('tr')
      .contains('Lagardelle')
  })
})
