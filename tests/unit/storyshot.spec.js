import initStoryshots, { multiSnapshotWithOptions } from '@storybook/addon-storyshots'

initStoryshots({
  framework: 'vue3',
  suite: 'storyshots',
  test: multiSnapshotWithOptions()
})
